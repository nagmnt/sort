#----------------------
ary = [5, 3, 2, 4, 8, 0, 9, 6, 7 ,1]
n = len(ary)
#----------------------

def quicksort(left, right):
    if (left < right):
        i = left
        j = right
        pivot = ary[(j+i) // 2]
        #print("pivot",pivot)

        while True:
            while ary[i] < pivot:
                i += 1
            while pivot < ary[j]:
                j += -1
            if i >= j:
                break
            else:
                ary[i], ary[j] = ary[j], ary[i]
                i += 1
                j += -1
        #print(left,j)
        #print(i, right)
        quicksort(left, i-1)
        quicksort(j+1, right)

quicksort(0, n-1)
print(ary)
