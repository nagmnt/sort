#----------------------
ary = [5, 3, 2, 4, 8, 0, 9, 6, 7 ,1]
n = len(ary)
#----------------------

for i in range(n):
    min = i # min is not a value, but an index
    for j in range(i,n):
        if ary[j] < ary[min]:
            min = j
    ary[i], ary[min] = ary[min], ary[i] #swap

print(ary)

"""
ave case  : O(n^2)
worse case: O(n^2)
best case : O(n^2)
"""

