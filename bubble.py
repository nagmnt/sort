#----------------------
ary = [5, 3, 2, 4, 8, 0, 9, 6, 7 ,1]
n = len(ary)
#----------------------

for i in range(n):
    for j in range(0, n-i-1):
        if ary[j] > ary[j+1]:
            ary[j], ary[j+1] = ary[j+1], ary[j]

print(ary)

"""
ave case  : O(n^2)
worse case: O(n^2)
best case : O(n)
"""
