#----------------------
ary = [5, 3, 2, 4, 8, 0, 9, 6, 7 ,1]
n = len(ary)
#----------------------

def merge(left, right):
    mlist = []
    while len(left)*len(right) > 0:
        if left[0] < right[0]:
            mlist.append(left[0])
            left.pop(0)
        else:
            mlist.append(right[0])
            right.pop(0)
    if len(left) == 0:
        mlist += right
    else:
        mlist += left
    return mlist

def dif(list):
    if len(list) <= 1:
        return list
    mid = len(list) // 2
    left = list[:mid]
    right = list[mid:]
    return merge(dif(left), dif(right))

print(dif(ary))

"""
ave case  : O(nlogn)
worse case: O(nlogn)
best case : O(nlogn)
"""